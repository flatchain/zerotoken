#include "ciphertoken.hpp"
#include <eosio/crypto.hpp>
#include <eosio/permission.hpp>

char * bin_to_hex(const unsigned char *bin, size_t binsz, char *result, size_t hexsz)
{
    unsigned char hex_str[]= "0123456789abcdef";
    unsigned int i;
    
    if (hexsz < binsz * 2) return NULL;

    if (!binsz) return NULL;
    
    for (i = 0; i < binsz; i++) {
        result[i * 2 + 0] = hex_str[(bin[i] >> 4) & 0x0F];
        result[i * 2 + 1] = hex_str[(bin[i]     ) & 0x0F];
    }
    return result;
}

int get_hex(char c)
{
    if(c >= '0' && c<= '9')
        return c - '0';
    else if(c >= 'a' && c<= 'f')
        return c - 'a' + 10;
    else if(c >= 'A' && c<= 'F')
        return c - 'A' + 10;
    else
        return -1;//error
}

unsigned char* hex_to_bin(const char* hexStr, size_t hexsz, unsigned char * result, size_t binsz)
{
    if (binsz * 2 < hexsz) return NULL;

    unsigned long hexLen = strlen(hexStr);
    for(int i = 0; i < hexLen; i +=2)
    {
        result[i/2] = (get_hex(hexStr[i])*16 + get_hex(hexStr[i+1]));
    }
    
    return result;
}

void ciphertoken::sub_balance(const name& owner, const asset& value) {
   accounts from_acnts( get_self(), owner.value );
   const auto& from = from_acnts.get( value.symbol.code().raw(), "no balance object found");
   check(from.balance.amount >= value.amount, "overdraw balance");

   from_acnts.modify(from, owner, [&](auto& a){
      a.balance -= value;
   });
}

void ciphertoken::add_balance( const name& owner, const asset& value, const name& ram_payer ) {
   accounts to_acnts( get_self(), owner.value );
   auto to = to_acnts.find(value.symbol.code().raw());
   if (to == to_acnts.end()) {
      to_acnts.emplace(ram_payer, [&](auto& a){
         a.balance = value;
      });
   } else {
      to_acnts.modify(to, same_payer, [&](auto& a){
         a.balance += value;
      });
   }
}

void ciphertoken::add_cipher( const name& owner, const symbol& symbol, const string& value, const name& ram_payer) {
   accounts to_acnts( get_self(), owner.value );
   auto to = to_acnts.find(symbol.code().raw());
   if (to == to_acnts.end()) {
      to_acnts.emplace(ram_payer, [&](auto& a){
         a.balance = asset(0, symbol);
         a.cipher = value;
      });
   } else {
      to_acnts.modify(to, same_payer, [&](auto& a){
         if (a.cipher.empty()) {
            a.cipher = value;
         } else {
            eosio::cipher128 new_cipher = cipher_add(a.cipher.c_str(), a.cipher.size(), value.c_str(), value.size());
            char s[257] = { 0 };
            bin_to_hex((const unsigned char*)new_cipher.data(), 128,s, 256 );
            a.cipher = s;
         }
      });
   }
}

void ciphertoken::symbol_check(const asset& quantity, const symbol& symbol) {
   stats tb_stats( get_self(), symbol.code().raw() );
    const auto& st = tb_stats.get( symbol.code().raw() );
    check( quantity.symbol == st.supply.symbol, "symbol precision mismatch" );
}

void ciphertoken::cipher_transfer_check(const name& from, const name& to, const symbol& symbol, const string& fpubkey, const string& tpubkey, const string& eq_para, const string& from_pub_para, const string& to_pub_para, const string& signature) {
   require_auth(from);
	check(is_account(to), "to account does not exist");

   require_recipient( from );
	require_recipient( to );

   // verify ecdsa signature
   assert_cipher_ecdsa_signature(eq_para.c_str(), eq_para.size(), from_pub_para.c_str(), from_pub_para.size(), to_pub_para.c_str(), to_pub_para.size(), signature.c_str(), signature.size() );

   // verify equal prove
   accounts from_acnts( get_self(), from.value);
   const auto& f = from_acnts.get( symbol.code().raw(), "no balance object found");
   assert_cipher_equal_prove(f.cipher.c_str(), f.cipher.size() , eq_para.c_str(), eq_para.size() , fpubkey.c_str(), fpubkey.size() );

   // verify from pubkey prove and bulletproofs
   assert_cipher_valid_prove(from_pub_para.c_str(), from_pub_para.size() , fpubkey.c_str(), fpubkey.size() );

   // verify to pubkey prove and bulletproofs
   assert_cipher_valid_prove(to_pub_para.c_str(), to_pub_para.size() , tpubkey.c_str(), tpubkey.size() );
}

void ciphertoken::get_owners_key(const name& owner, string& hexkey) {
   char key[34] = { 0 };
   char hex[67] = { 0 };
   check(get_permission_key( owner, "active"_n, key, 34) == 34, "unknown error while getting user's active key");
   bin_to_hex((const unsigned char*)&key[1], 33, hex, 66);
   hexkey = string(hex);
}

ACTION ciphertoken::create( const name& issuer, const asset& maximum_supply) {
   require_auth( get_self() );

   auto sym = maximum_supply.symbol;
   check(sym.is_valid(), "invalid symbol name");
   check(maximum_supply.is_valid(), "invalid supply");
   check(maximum_supply.amount > 0, "max-supply must be positive");

   stats tb_stats( get_self(), sym.code().raw() );
   auto existing = tb_stats.find( sym.code().raw() );
   check( existing == tb_stats.end(), "token with symbol already exists" );

   tb_stats.emplace( get_self(), [&](auto& s) {
      s.supply.symbol = sym;
      s.max_supply = maximum_supply;
      s.issuer = issuer;
   });
}

ACTION ciphertoken::issue(const name& to, const asset& quantity, const string& memo) {
   auto sym = quantity.symbol;
   check(sym.is_valid(), "invalid symbol name");
   check(memo.size() <= 256, "memo has more than 256 bytes");

   stats tb_stats( get_self(), sym.code().raw() );
   auto existing = tb_stats.find(sym.code().raw());
   check(existing != tb_stats.end(), "token with symbol does not exist, create token before issue");
   const auto& st = *existing;
   check( to == st.issuer, "tokens can only be issued to issuer account" );

   require_auth( st.issuer );
   check( quantity.is_valid(), "invalid quantity" );
   check( quantity.amount > 0, "must issue positive quantity" );

   check(quantity.symbol == st.supply.symbol, "symbol precision mismatch");
   check(quantity.amount <= st.max_supply.amount - st.supply.amount, "quantity exceeds available supply");

   tb_stats.modify( st, same_payer, [&](auto& s){
      s.supply += quantity;
   });

   add_balance(st.issuer, quantity, st.issuer);
}

ACTION ciphertoken::retire(const asset& quantity, const string& memo) {
   auto sym = quantity.symbol;
   check(sym.is_valid(), "invalid symbol name");
   check(memo.size() <= 256, "memo has more than 256 bytes");

   stats tb_stats( get_self(), sym.code().raw());
   auto existing = tb_stats.find(sym.code().raw());
   check(existing != tb_stats.end(), "token with symbol does not exist");
   const auto& st = *existing;

   require_auth(st.issuer);
   check(quantity.is_valid(), "invalid quantity");
   check(quantity.amount > 0, "must retire positive quantity");

   check(quantity.symbol == st.supply.symbol, "symbol precision mismatch");

   tb_stats.modify(st, same_payer, [&](auto& a){
      a.supply -= quantity;
   });

   sub_balance(st.issuer, quantity);
}

ACTION ciphertoken::transferpp(const name& from, const name& to, const asset& quantity, const string& memo) {
    check( from != to, "cannot transfer to self" );
    require_auth( from );
    check( is_account( to ), "to account does not exist");
    auto sym = quantity.symbol.code();
    stats tb_stats( get_self(), sym.raw() );
    const auto& st = tb_stats.get( sym.raw() );

    require_recipient( from );
    require_recipient( to );

    check( quantity.is_valid(), "invalid quantity" );
    check( quantity.amount > 0, "must transfer positive quantity" );
    check( quantity.symbol == st.supply.symbol, "symbol precision mismatch" );
    check( memo.size() <= 256, "memo has more than 256 bytes" );

    auto payer = has_auth( to ) ? to : from;

    sub_balance( from, quantity );
    add_balance( to, quantity, payer );
}

ACTION ciphertoken::transferpc(const name& from, const name& to, const symbol& symbol, const asset& quantity, const string& random, const string& ciphertext, const string& memo) {
   require_auth( from );
   check(is_account(to), "to account does not exist");

   symbol_check(quantity, symbol);

   require_recipient( from );
   require_recipient( to );

   check(quantity.is_valid(), "invalid quantity");
   check(quantity.amount > 0, "must transfer positive quantity");
   check(quantity.amount <= UINT32_MAX, "must transfer less than 4294967295 quantity");
   check(memo.size() <= 256, "memo has more than  256 bytes");

   string tkey;
   get_owners_key(to, tkey);
   // verify ciphertext match (tpubkey, quantity, random)
   assert_cipher_encrypt(random.c_str(), random.size(), (unsigned int)quantity.amount, tkey.c_str(), tkey.size(), ciphertext.c_str(), ciphertext.size());

   auto payer = has_auth(to) ? to : from;
   sub_balance(from, quantity);
   add_cipher(to, symbol, ciphertext, payer);
}

ACTION ciphertoken::transfercc( const name& from, const name& to, const symbol& symbol, const string& eq_para, const string& from_pub_para, const string& to_pub_para, const string& signature, const string& memo ) {
   check( from != to, "cannot transfer to self" );
   string fkey, tkey;
   get_owners_key(from, fkey);
   get_owners_key(to, tkey);

   cipher_transfer_check(from, to, symbol, fkey, tkey, eq_para, from_pub_para, to_pub_para, signature);
   check(memo.size() <= 256, "memo has more than  256 bytes");

   // Update from and to's cipher balance
   accounts from_acnts( get_self(), from.value);
   const auto& f = from_acnts.get( symbol.code().raw(), "no balance object found");
   from_acnts.modify(f, from, [&](auto& a){
      a.cipher = from_pub_para.substr(0, 256);
   });

   auto payer = has_auth(to) ? to : from;
   add_cipher(to, symbol, to_pub_para.substr(0, 256), payer);
}

ACTION ciphertoken::transfercp(const name& from, const name& to, const symbol& symbol, const asset& quantity, const string& random, const string& eq_para, const string& from_pub_para, const string& to_pub_para, const string& signature, const string& memo) {
   string fkey, tkey;
   get_owners_key(from, fkey);
   get_owners_key(to, tkey);

   cipher_transfer_check(from, to, symbol, fkey, tkey, eq_para, from_pub_para, to_pub_para, signature);
   assert_cipher_encrypt(random.c_str(), random.size(), (unsigned int)quantity.amount, tkey.c_str(), tkey.size(), to_pub_para.substr(0, 256).c_str(), 256);

   check(quantity.is_valid(), "invalid quantity");
   check(quantity.amount > 0, "must transfer positive quantity");
   check(quantity.amount <= UINT32_MAX, "must transfer less than 4294967295 quantity");
   check(memo.size() <= 256, "memo has more than  256 bytes");
   symbol_check(quantity, symbol);

   accounts from_acnts( get_self(), from.value);
   const auto& f = from_acnts.get( symbol.code().raw(), "no balance object found");
   from_acnts.modify(f, from, [&](auto& a){
      a.cipher = from_pub_para.substr(0, 256);
   });

   auto payer = has_auth( to ) ? to : from;
   add_balance( to, quantity, payer );
}

ACTION ciphertoken::open(const name& owner, const symbol& symbol, const name& ram_payer) {
   require_auth( ram_payer );

   check( is_account( owner ), "owner account does not exist" );

   auto sym_code_raw = symbol.code().raw();
   stats statstable( get_self(), sym_code_raw );
   const auto& st = statstable.get( sym_code_raw, "symbol does not exist" );
   check( st.supply.symbol == symbol, "symbol precision mismatch" );

   accounts acnts( get_self(), owner.value );
   auto it = acnts.find( sym_code_raw );
   if( it == acnts.end() ) {
      acnts.emplace( ram_payer, [&]( auto& a ){
        a.balance = asset{0, symbol};
      });
   }
}

ACTION ciphertoken::close(const name& owner, const symbol& symbol) {
   require_auth( owner );
   accounts acnts( get_self(), owner.value );
   auto it = acnts.find( symbol.code().raw() );
   check( it != acnts.end(), "Balance row already deleted or never existed. Action won't have any effect." );
   check( it->balance.amount == 0, "Cannot close because the balance is not zero." );
   acnts.erase( it );
}

ACTION ciphertoken::foo()
{
	require_auth(_self);

   char key[34] = { 0 };
   int p = get_permission_key( get_self(), "active"_n, key, 34);

   char hex[67] = { 0 };
   bin_to_hex((const unsigned char*)&key[1], 33, hex, 66);

   check(false, std::string(hex));

	// string c_a = "68e294b9520ccfdeffb6a2091c407beb477f5bd3cb411df422f220589c0ec57d7bca90af21f4e617a895be9a12d2254a776001be4063efb320ab539f3262883d3cb379eec4eb69ca08626b0ed3c0ffe237d37abd84141ebb4cf99d8f397032c96565b72e11a5f52d33c7a16b163b29f4819b24471deceaa2708498cd8997987c";
	// accounts acnts(_self, _self.value);
	// auto c = acnts.find("holyshitmans"_n.value);
	// if (c == acnts.end()) {
	// 	acnts.emplace(_self, [&](auto &a){
	// 		a.id = "holyshitmans"_n;
	// 		a.cipher = c_a;
	// 	});
	// } else {
	// 	acnts.modify(*c, _self, [&](auto &a){
	// 		a.cipher = c_a;
	// 	});
	// }

	// string c_b = "8786d19e474007a79e9e8276ad66bec663071754bc091c75f4a7f39db887c7315059783c82b522e4664dd2c6a07e04d5c6720c56e65b954bd5bbe32a02c559cc2b2c905a4565e829ea178da7c173fa70f8452e13c2d329ee46c1753a3dcde54cd8823507682bd0366bd2af08ea7506c2537cd6392315a7f773314e957c2dda1a";
	// c = acnts.find("ciphertext11"_n.value);
	// if (c == acnts.end()) {
	// 	acnts.emplace(_self, [&](auto &a){
	// 		a.id = "ciphertext11"_n;
	// 		a.cipher = c_b;
	// 	});
	// } else {
	// 	acnts.modify(*c, _self, [&](auto &a){
	// 		a.cipher = c_b;
	// 	});
	// }
}
