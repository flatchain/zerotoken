#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>
#include <string>

using namespace eosio;
using namespace std;

CONTRACT ciphertoken : public contract {
public:
      using contract::contract;

      ACTION create( const name& issuer, const asset& maximum_supply);

      ACTION issue(const name& to, const asset& quantity, const string& memo);

      ACTION retire(const asset& quantity, const string& memo);

      ACTION transferpp(const name& from, const name& to, const asset& quantity, const string& memo);

      ACTION transferpc(const name& from, const name& to, const symbol& symbol, const asset& quantity, const string& random, const string& ciphertext, const string& memo);

      ACTION transfercc(const name& from, const name& to, const symbol& symbol, const string& eq_para, const string& from_pub_para, const string& to_pub_para, const string& signature, const string& memo );

      ACTION transfercp(const name& from, const name& to, const symbol& symbol, const asset& quantity, const string& random, const string& eq_para, const string& from_pub_para, const string& to_pub_para, const string& signature, const string& memo);

      ACTION open(const name& owner, const symbol& symbol, const name& ram_payer);

      ACTION close(const name& owner, const symbol& symbol);

      ACTION foo();

      using create_action = action_wrapper<"create"_n, &ciphertoken::create>;
      using issue_action = action_wrapper<"issue"_n, &ciphertoken::issue>;
      using retire_action = action_wrapper<"retire"_n, &ciphertoken::retire>;
      using transferpp_action = action_wrapper<"transferpp"_n, &ciphertoken::transferpp>;
      using transferpc_action = action_wrapper<"transferpc"_n, &ciphertoken::transferpc>;
      using transfercc_action = action_wrapper<"transfercc"_n, &ciphertoken::transfercc>;
      using transfercp_action = action_wrapper<"transfercp"_n, &ciphertoken::transfercp>;
      using open_action = action_wrapper<"open"_n, &ciphertoken::open>;
      using close_action = action_wrapper<"close"_n, &ciphertoken::close>;

private:
      struct [[eosio::table]] account {
         asset    balance;
         string   cipher;

         uint64_t primary_key()const { return balance.symbol.code().raw(); }
      };

      struct [[eosio::table]] currency_stats {
         asset    supply;
         asset    max_supply;
         name     issuer;

         uint64_t primary_key()const { return supply.symbol.code().raw(); }
      };

      typedef eosio::multi_index< "accounts"_n, account > accounts;
      typedef eosio::multi_index< "stat"_n, currency_stats > stats;

      void sub_balance( const name& owner, const asset& value );
      void add_balance( const name& owner, const asset& value, const name& ram_payer );

      void add_cipher( const name& owner, const symbol& symbol, const string& value, const name& ram_payer);

      void symbol_check(const asset& quantity, const symbol& symbol);

      void cipher_transfer_check(const name& from, const name& to, const symbol& symbol, const string& fpubkey, const string& tpubkey, const string& eq_para, const string& from_pub_para, const string& to_pub_para, const string& signature);

      void get_owners_key(const name& owner, string& hexkey);
};